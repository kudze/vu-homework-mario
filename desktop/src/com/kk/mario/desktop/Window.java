package com.kk.mario.desktop;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class Window {
    private String title;
    private int width;
    private int height;
    private boolean resizeable;

    public Window(String title, int width, int height, boolean resizeable) {
        this.title = title;
        this.width = width;
        this.height = height;
        this.resizeable = resizeable;
    }

    public LwjglApplication startApplication(ApplicationAdapter application)
    {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.width = this.width;
        config.height = this.height;
        config.resizable = this.resizeable;
        config.title = this.title;

        return new LwjglApplication(application, config);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public boolean isResizeable() {
        return resizeable;
    }

    public void setResizeable(boolean resizeable) {
        this.resizeable = resizeable;
    }
}
