
var timeExecuted = 0;
var first = true;
var state = 0;
function update() {
    timeExecuted += API.getDeltaTime();

    API.print("Pos: " + API.getMarioX() + ":" + API.getMarioY() + " | State: " + state);

    switch(state) {
        case 0: {
            API.setMarioGoRight(true);

            if(API.getMarioX() >= 7)
            {
                API.stabilize();
                state++;
            }

            break;
        }

        case 1: {
            if(!API.hasStabilizedPoint())
            {
                API.marioJump();
                state++;
            }

            break;
        }
        case 2: {
            if(API.isOnGround())
            {
                API.setMarioGoRight(true);
                state++;
            }

            break;
        }
        case 3: {
            if(API.getMarioX() >= 8)
            {
                API.stabilize();
                state++;
            }

            break;
        }
        case 4: {
            if(!API.hasStabilizedPoint())
            {
                API.marioJump();
                state++;
            }

            break;
        }
        case 5: {
            if(API.isOnGround())
            {
                API.setMarioGoRight(true);
                state++;
            }

            break;
        }
        case 6: {
            if(API.getMarioX() >= 9)
            {
                API.stabilize();
                state++;
            }

            break;
        }
        case 7: {
            if(!API.hasStabilizedPoint())
            {
                API.marioJump();
                state++;
            }

            break;
        }
        case 8: {
            if(API.isOnGround())
            {
                API.setMarioGoLeft(true);
            }

            if(API.getMarioX() <= 1)
            {
                API.setMarioGoLeft(false);
                API.setMarioGoRight(true);
                state++;
            }

            break;
        }
        case 9: {
            if(API.getMarioX() >= 3 && API.getMarioX() <= 4)
            {
                API.marioJump();
            }

            break;
        }
    }
}
