var stabilizedAt = null;
var state = 0;


function update() {
    API.print('x ' + API.getMarioX() + ' | y ' + API.getMarioY());

    if(state === 1)
    {
        API.setMarioGoRight(true);
        API.marioJump();

        return;
    }

    if (API.getMarioX() >= 18) {
        if (API.hasStabilizedPoint()) {

        } else if (stabilizedAt != null) {

            if(API.isOnGround())
            {
                API.setMarioGoRight(true);
                API.setMarioGoLeft(false);

                if(API.getMarioX() >= 19)
                {
                    API.marioJump();
                    state++;
                }
            }

        } else {
            API.stabilize();
            stabilizedAt = 1;
        }
    } else {
        if (API.getMarioX() >= 3 && stabilizedAt === null)
            API.marioJump();
    }

    API.setMarioGoRight(true);
}
