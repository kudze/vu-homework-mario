
function init() {
    API.print('on init');
}

var __state = 0;

function update() {

    var state = __state;
    API.print("Pos: " + API.getMarioX() + ":" + API.getMarioY() + " | State: " + state);

    switch (state) {
        case 0: {
            if(!API.isOnGround())
                return;

            API.marioJump();
            API.setMarioGoRight(true);
            __state++;

            break;
        }

        case 1: {
            API.marioJump();
            API.setMarioGoRight(true);

            if(API.getMarioX() >= 39.0) {
                API.stabilize();
                __state++;
            }

            break;
        }

        case 2: {
            if(!API.hasStabilizedPoint())
            {
                API.marioJump();
                API.setMarioGoRight(true);

                if(API.getMarioX() >= 43.0)
                {
                    API.stabilize();
                    __state++;
                }
            }

            break;
        }

        case 3: {

            API.setMarioGoRight(true);

            if(API.getMarioX() >= 45.0)
            {
                API.marioJump();
                __state++;
            }

            break;
        }

        case 4: {
            break;
        }
    }

}

function dispose() {
    //API.print('on dispose');
}
