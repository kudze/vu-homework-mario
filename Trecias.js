function init() {
    API.print('on init');
}

var __state = 0;

function update() {

    var state = __state;
    API.print("Pos: " + API.getMarioX() + ":" + API.getMarioY() + " | State: " + state);

    switch (state) {
        case 0: {
            if (!API.isOnGround())
                return;

            API.setMarioGoRight(true);
            if (API.getMarioX() >= 5 && API.getMarioX() <= 6) {
                API.marioJump();
            }

            if (API.getMarioX() >= 24) {
                API.stabilize();
                __state++;
            }

            break;
        }

        case 1: {
            if (!API.hasStabilizedPoint()) {
                API.marioJump();
                API.setMarioGoLeft(true);

                if (API.getMarioX() <= 23) {
                    API.stabilize();
                    __state++;
                }
            }

            break;
        }

        case 2: {
            if (!API.hasStabilizedPoint()) {
                API.marioJump();
                API.setMarioGoRight(true);
                __state++;
            }

            break;
        }

        case 3: {
            if (API.getMarioX() >= 26) {
                API.stabilize();
                __state++;
            }

            break;
        }

        case 4: {
            if (!API.hasStabilizedPoint()) {
                API.setMarioGoLeft(true);
                API.setMarioGoRight(false);

                if (API.isOnGround()) {
                    __state++;
                }
            }

            break;
        }

        case 5: {
            if (API.getMarioX() < 15 && API.isOnGround()) {
                API.stabilize();
                __state++;
            }

            if (API.getMarioX() <= 25.1) {
                API.marioJump();
            }

            break;
        }

        case 6: {
            if (!API.hasStabilizedPoint()) {
                API.marioJump();
                API.setMarioGoRight(true);
            }

            if (API.getMarioX() === 68) {
                API.setMarioGoRight(false);
                API.setMarioGoLeft(true);
                API.marioJump();
                __state++;
            }

            break;
        }

        case 7: {
            API.marioJump();

            if(API.getMarioX() <= 57)
            {
                API.setMarioGoLeft(false);
                API.setMarioGoRight(true);
            }

            if (API.getMarioX() <= 56.2) {
                API.stabilize();
                __state++;
            }

            break;
        }

        case 8: {
            if (!API.hasStabilizedPoint()) {
                API.setMarioGoLeft(true);

                if(API.getMarioX() >= 55 && API.getMarioX() <= 55.5)
                {
                    API.marioJump();
                }

                if(API.getMarioX() <= 47.5)
                {
                    API.marioJump();
                }

                if(API.getMarioX() == 30)
                    __state++;
            }

            break;
        }

        case 9: {
            API.setMarioGoLeft(true);
            API.setMarioGoRight(false);
            API.marioJump();
        }
    }

}

function dispose() {
    //API.print('on dispose');
}
