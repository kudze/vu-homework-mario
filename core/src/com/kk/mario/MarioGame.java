package com.kk.mario;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.kk.mario.states.GameStateManager;
import com.kk.mario.states.game.LevelState;
import com.kk.mario.states.main_menu.MainMenuState;

public class MarioGame extends ApplicationAdapter {
	private static MarioGame instance = null;
	public static MarioGame getInstance() {
		if(MarioGame.instance == null) {
			MarioGame.instance = new MarioGame();
		}

		return MarioGame.instance;
	}

	private GameStateManager gameStateManager;

	@Override
	public void create () {
		this.gameStateManager = new GameStateManager(new MainMenuState());
	}

	@Override
	public void render () {
		this.gameStateManager.getActiveGameState().update();
		this.clearFrame();
		this.gameStateManager.getActiveGameState().render();

		if(this.gameStateManager.getActiveGameState() instanceof LevelState)
		{
			LevelState levelState = (LevelState) this.gameStateManager.getActiveGameState();
			if(levelState.getMario().isDead() && levelState.getMario().getPosition().y < -32)
				this.gameStateManager.setActiveGameState(new MainMenuState());

			if(levelState.getMario().isOnFlag() && !levelState.getMario().canGoDown())
				this.gameStateManager.setActiveGameState(new MainMenuState());
		}
	}
	
	@Override
	public void dispose () {
		this.gameStateManager.dispose();
	}

	private void clearFrame()
	{
		Gdx.gl.glClearColor(0, 0.56f, 0.77f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
	}

	public GameStateManager getGameStateManager() {
		return gameStateManager;
	}
}
