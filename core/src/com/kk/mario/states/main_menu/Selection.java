package com.kk.mario.states.main_menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.kk.mario.rendering.text.Font;

import java.util.ArrayList;

public class Selection {

    private ArrayList<Option> options;
    private int activeIndex = 0;

    private Font font;
    private Font highlightedFont;

    public Selection(Font font, Font highlightedFont)
    {
        this(new ArrayList<Option>(), font, highlightedFont);
    }

    public Selection(ArrayList<Option> options, Font font, Font highlightedFont) {
        this.options = options;
        this.font = font;
        this.highlightedFont = highlightedFont;
    }

    public void update() {
        if(Gdx.input.isKeyJustPressed(Input.Keys.UP))
        {
            int activeIndex = (this.getActiveIndex() - 1) % this.options.size();

            if(activeIndex == -1)
                activeIndex = this.options.size() - 1;

            this.setActiveIndex(activeIndex);
        }

        if(Gdx.input.isKeyJustPressed(Input.Keys.DOWN))
        {
            int activeIndex = (this.getActiveIndex() + 1) % this.options.size();

            this.setActiveIndex(activeIndex);
        }

        if(Gdx.input.isKeyJustPressed(Input.Keys.ENTER))
        {
            this.getActiveOption().execute();
        }
    }

    public void render(Batch batch, int startX, int startY) {
        int currentIDX = 0;
        for (Option option : this.options) {
            if(currentIDX == this.getActiveIndex())
                this.highlightedFont.getInstance().draw(batch, option.getTitle(), startX, startY);
            else
                this.font.getInstance().draw(batch, option.getTitle(), startX, startY);

            startY -= 50;
            currentIDX++;
        }
    }

    public void dispose() {
        this.font.dispose();
        this.highlightedFont.dispose();
    }

    public ArrayList<Option> getOptions() {
        return options;
    }

    public void addOption(Option option) {
        options.add(option);
    }

    public void setOptions(ArrayList<Option> options) {
        this.options = options;
    }

    public Option getActiveOption() {
        return this.options.get(this.activeIndex);
    }

    public int getActiveIndex() {
        return activeIndex;
    }

    public void setActiveIndex(int activeIndex) {
        if(activeIndex < 0 || activeIndex >= this.options.size())
            throw new IllegalArgumentException("Selection active index must be in the range of [0, " + (this.options.size() - 1) + "]");

        this.activeIndex = activeIndex;
    }
}
