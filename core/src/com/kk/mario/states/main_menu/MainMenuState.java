package com.kk.mario.states.main_menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.kk.mario.MarioGame;
import com.kk.mario.rendering.image.Picture;
import com.kk.mario.rendering.text.Font;
import com.kk.mario.states.GameState;
import com.kk.mario.states.game.*;

public class MainMenuState implements GameState {
    private SpriteBatch batch;
    private Font bigFont;
    private Picture background;
    private Selection selection;

    public MainMenuState() {
        this.batch = new SpriteBatch();
        this.background = new Picture(Gdx.files.internal("img/main_menu_bg.jpg"), 1280, 720);
        this.bigFont = new Font(Gdx.files.internal("fonts/SuperMario256.ttf"), 100, Color.WHITE, Color.BLACK, 4);

        this.selection = new Selection(
                new Font(Gdx.files.internal("fonts/SuperMario256.ttf"), 50, Color.WHITE, Color.BLACK, 2),
                new Font(Gdx.files.internal("fonts/SuperMario256.ttf"), 50, Color.RED, Color.BLACK, 2)
        );

        this.selection.addOption(new Option("1st Level") {
            @Override
            public void execute() {
                MarioGame.getInstance().getGameStateManager().markForChange();
                MarioGame.getInstance().getGameStateManager().setActiveGameState(new FirstGameLevel());
            }
        });

        this.selection.addOption(new Option("2nd Level") {
            @Override
            public void execute() {
                MarioGame.getInstance().getGameStateManager().markForChange();
                MarioGame.getInstance().getGameStateManager().setActiveGameState(new SecondGameLevel());
            }
        });

        this.selection.addOption(new Option("3rd Level") {
            @Override
            public void execute() {
                MarioGame.getInstance().getGameStateManager().markForChange();
                MarioGame.getInstance().getGameStateManager().setActiveGameState(new ThirdGameLevel());
            }
        });

        this.selection.addOption(new Option("1st Tutorial Level") {
            @Override
            public void execute() {
                MarioGame.getInstance().getGameStateManager().markForChange();
                MarioGame.getInstance().getGameStateManager().setActiveGameState(new FirstTutorialLevel());
            }
        });

        this.selection.addOption(new Option("2nd Tutorial Level") {
            @Override
            public void execute() {
                MarioGame.getInstance().getGameStateManager().markForChange();
                MarioGame.getInstance().getGameStateManager().setActiveGameState(new SecondTutorialLevel());
            }
        });

        this.selection.addOption(new Option("3rd Tutorial Level") {
            @Override
            public void execute() {
                MarioGame.getInstance().getGameStateManager().markForChange();
                MarioGame.getInstance().getGameStateManager().setActiveGameState(new ThirdTutorialLevel());
            }
        });

        this.selection.addOption(new Option("4th Tutorial Level") {
            @Override
            public void execute() {
                MarioGame.getInstance().getGameStateManager().markForChange();
                MarioGame.getInstance().getGameStateManager().setActiveGameState(new FourthTutorialLevel());
            }
        });

        this.selection.addOption(new Option("5th Tutorial Level") {
            @Override
            public void execute() {
                MarioGame.getInstance().getGameStateManager().markForChange();
                MarioGame.getInstance().getGameStateManager().setActiveGameState(new FifthTutorialLevel());
            }
        });

        this.selection.addOption(new Option("6th Tutorial Level") {
            @Override
            public void execute() {
                MarioGame.getInstance().getGameStateManager().markForChange();
                MarioGame.getInstance().getGameStateManager().setActiveGameState(new SixthTutorialLevel());
            }
        });

        this.selection.addOption(new Option("7th Tutorial Level") {
            @Override
            public void execute() {
                MarioGame.getInstance().getGameStateManager().markForChange();
                MarioGame.getInstance().getGameStateManager().setActiveGameState(new SeventhTutorialLevel());
            }
        });

        this.selection.addOption(new Option("Quit") {
            @Override
            public void execute() {
                MarioGame.getInstance().dispose();
                Gdx.app.exit();
                System.exit(0);
            }
        });
    }

    @Override
    public void update() {
        this.selection.update();
    }

    @Override
    public void render() {
        batch.begin();
        this.background.getInstance().draw(batch, 1);
        this.bigFont.getInstance().draw(batch, "mario game", 50, 710);
        this.selection.render(batch, 50, 565);

        batch.end();
    }

    @Override
    public void dispose() {
        this.selection.dispose();
        this.bigFont.dispose();
        this.background.dispose();
        this.batch.dispose();
    }

}
