package com.kk.mario.states.main_menu;

public abstract class Option {
    private String title;

    public Option(String title) {
        this.title = title;
    }

    public abstract void execute();

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
