package com.kk.mario.states;

public interface GameState {

    public void update();
    public void render();
    public void dispose();

}
