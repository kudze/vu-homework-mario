package com.kk.mario.states.game;

import com.kk.mario.states.game.entities.enemies.Mushroom;
import com.kk.mario.states.game.entities.enemies.Smile;
import com.kk.mario.states.game.map.Flag;
import com.kk.mario.states.game.map.Map;
import com.kk.mario.states.game.map.Stone;

public class SeventhTutorialLevel extends LevelState {

    public SeventhTutorialLevel() {
        super(
                new Map(20, 12),
                2, 1,
                "Seventh_Tutorial"
        );

        for (int i = 0; i < 20; i++) {
            this.getMap().setBlockAtXY(
                    i,
                    11,
                    new Stone(this.getBlocksTextureAtlas())
            );

            this.getMap().setBlockAtXY(
                    i,
                    0,
                    new Stone(this.getBlocksTextureAtlas())
            );
        }

        for (int i = 0; i < 12; i++) {
            this.getMap().setBlockAtXY(
                    0,
                    i,
                    new Stone(this.getBlocksTextureAtlas())
            );

            this.getMap().setBlockAtXY(
                    19,
                    i,
                    new Stone(this.getBlocksTextureAtlas())
            );
        }

        this.getMap().setBlockAtXY(5, 1, new Stone(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(9, 1, new Stone(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(10, 1, new Stone(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(11, 1, new Stone(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(15, 1, new Stone(this.getBlocksTextureAtlas()));

        this.addEnemy(new Mushroom(this.getEnemiesTextureAtlas(), 6, 1));
        this.addEnemy(new Mushroom(this.getEnemiesTextureAtlas(), 7, 1));
        this.addEnemy(new Mushroom(this.getEnemiesTextureAtlas(), 8, 1));
        this.addEnemy(new Smile(this.getEnemiesTextureAtlas(), 12, 1));
        this.addEnemy(new Smile(this.getEnemiesTextureAtlas(), 14, 1));
        this.addEnemy(new Smile(this.getEnemiesTextureAtlas(), 15, 1));

        this.getMap().setBlockAtXY(17, 1, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(17, 2, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(17, 3, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(17, 4, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(17, 5, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(17, 6, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(17, 7, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(17, 8, new Flag(this.getBlocksTextureAtlas(), true));
    }

}
