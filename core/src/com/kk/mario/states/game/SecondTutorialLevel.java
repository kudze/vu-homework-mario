package com.kk.mario.states.game;

import com.kk.mario.states.game.map.Brick;
import com.kk.mario.states.game.map.Flag;
import com.kk.mario.states.game.map.Map;
import com.kk.mario.states.game.map.Stone;

public class SecondTutorialLevel extends LevelState {

    public SecondTutorialLevel() {
        super(
                new Map(20, 12),
                2, 8,
                "Antras_Tutorial"
        );

        for (int i = 0; i < 20; i++) {
            this.getMap().setBlockAtXY(
                    i,
                    11,
                    new Stone(this.getBlocksTextureAtlas())
            );

            this.getMap().setBlockAtXY(
                    i,
                    0,
                    new Stone(this.getBlocksTextureAtlas())
            );
        }

        for (int i = 0; i < 12; i++) {
            this.getMap().setBlockAtXY(
                    0,
                    i,
                    new Stone(this.getBlocksTextureAtlas())
            );

            this.getMap().setBlockAtXY(
                    19,
                    i,
                    new Stone(this.getBlocksTextureAtlas())
            );
        }

        for(int i = 0; i < 5; i++)
        {
            this.getMap().setBlockAtXY(5, 6 + i, new Brick(this.getBlocksTextureAtlas()));

            if(i == 1)
                continue;

            this.getMap().setBlockAtXY(i + 1, 6, new Brick(this.getBlocksTextureAtlas()));
        }

        this.getMap().setBlockAtXY(17, 1, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(17, 2, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(17, 3, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(17, 4, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(17, 5, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(17, 6, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(17, 7, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(17, 8, new Flag(this.getBlocksTextureAtlas(), true));
    }

}
