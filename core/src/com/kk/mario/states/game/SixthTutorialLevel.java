package com.kk.mario.states.game;

import com.kk.mario.states.game.map.*;

public class SixthTutorialLevel extends LevelState {

    public SixthTutorialLevel() {
        super(
                new Map(40, 15),
                2, 1,
                "Sixth_Tutorial"
        );

        for (int i = 0; i < 40; i++) {
            this.getMap().setBlockAtXY(
                    i,
                    14,
                    new Stone(this.getBlocksTextureAtlas())
            );

            if(i <= 5 || i >= 35) {
                this.getMap().setBlockAtXY(
                        i,
                        0,
                        new Stone(this.getBlocksTextureAtlas())
                );
            }

            if(i == 5 || i == 35) {
                this.getMap().setBlockAtXY(
                        i,
                        1,
                        new Stone(this.getBlocksTextureAtlas())
                );
            }
        }

        for (int i = 0; i < 15; i++) {
            this.getMap().setBlockAtXY(
                    0,
                    i,
                    new Stone(this.getBlocksTextureAtlas())
            );

            this.getMap().setBlockAtXY(
                    39,
                    i,
                    new Stone(this.getBlocksTextureAtlas())
            );
        }

        for (int i = 0; i < 3; i++) {

            this.getMap().setBlockAtXY(8 + i, 3, new Brick(this.getBlocksTextureAtlas()));
            this.getMap().setBlockAtXY(11 + i, 6, new Brick(this.getBlocksTextureAtlas()));
            this.getMap().setBlockAtXY(18 + i, 3, new Brick(this.getBlocksTextureAtlas()));
            this.getMap().setBlockAtXY(21 + i, 6, new Brick(this.getBlocksTextureAtlas()));
            this.getMap().setBlockAtXY(25 + i, 9, new Brick(this.getBlocksTextureAtlas()));
            this.getMap().setBlockAtXY(25 + i, 3, new Brick(this.getBlocksTextureAtlas()));
            this.getMap().setBlockAtXY(28 + i, 6, new Brick(this.getBlocksTextureAtlas()));
            this.getMap().setBlockAtXY(32 + i, 3, new Brick(this.getBlocksTextureAtlas()));

        }



        this.getMap().setBlockAtXY(37, 1, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(37, 2, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(37, 3, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(37, 4, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(37, 5, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(37, 6, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(37, 7, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(37, 8, new Flag(this.getBlocksTextureAtlas(), true));
    }

}
