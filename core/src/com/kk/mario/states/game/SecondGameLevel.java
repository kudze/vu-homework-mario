package com.kk.mario.states.game;

import com.kk.mario.states.game.entities.enemies.Mushroom;
import com.kk.mario.states.game.entities.enemies.Smile;
import com.kk.mario.states.game.entities.mario.Mario;
import com.kk.mario.states.game.map.*;

public class SecondGameLevel extends LevelState {
    public SecondGameLevel()
    {
        super(new Map(60, 20), 2, 8, "Antras");

        for(int i = 0; i < 60; i++) {
            this.getMap().setBlockAtXY(
                    i,
                    19,
                    new Stone(this.getBlocksTextureAtlas())
            );

            if(!(i >= 8 && i <= 11) && !(i >= 36 && i <= 54))
            this.getMap().setBlockAtXY(
                    i,
                    0,
                    new Stone(this.getBlocksTextureAtlas())
            );
        }

        for(int i = 0; i < 20; i++) {
            this.getMap().setBlockAtXY(
                    0,
                    i,
                    new Stone(this.getBlocksTextureAtlas())
            );

            this.getMap().setBlockAtXY(
                    59,
                    i,
                    new Stone(this.getBlocksTextureAtlas())
            );
        }

        this.getMap().setBlockAtXY(7, 1, new Stone(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(7, 2, new Stone(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(6, 1, new Stone(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(12, 1, new Stone(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(12, 2, new Stone(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(13, 1, new Stone(this.getBlocksTextureAtlas()));

        this.addEnemy(new Mushroom(this.getEnemiesTextureAtlas(), 14, 1));
        this.addEnemy(new Smile(this.getEnemiesTextureAtlas(), 18, 1));

        this.getMap().setBlockAtXY(20, 1, new Stone(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(21, 5, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(21, 4, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(21, 3, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(21, 2, new Stone(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(21, 1, new Stone(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(22, 1, new Stone(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(22, 5, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(23, 5, new Question(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(24, 5, new Question(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(25, 5, new Question(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(26, 5, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(27, 5, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(27, 4, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(26, 1, new Stone(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(27, 1, new Stone(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(28, 1, new Stone(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(35, 1, new Stone(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(35, 2, new Stone(this.getBlocksTextureAtlas()));
        this.addEnemy(new Mushroom(this.getEnemiesTextureAtlas(), 24, 1));
        this.addEnemy(new Smile(this.getEnemiesTextureAtlas(), 30, 1));

        this.getMap().setBlockAtXY(39, 4, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(40, 4, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(43, 8, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(44, 8, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(45, 8, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(52, 6, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(51, 6, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(55, 2, new Stone(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(55, 1, new Stone(this.getBlocksTextureAtlas()));

        this.getMap().setBlockAtXY(58, 1, new Stone(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(57, 1, new Stone(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(56, 1, new Stone(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(57, 2, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(57, 3, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(57, 4, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(57, 5, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(57, 6, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(57, 7, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(57, 8, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(57, 9, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(57, 10, new Flag(this.getBlocksTextureAtlas(), true));
    }
}
