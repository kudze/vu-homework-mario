package com.kk.mario.states.game.code;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.kk.mario.MarioGame;
import com.kk.mario.states.GameState;
import com.kk.mario.states.game.LevelState;
import com.kk.mario.states.game.entities.enemies.Enemy;
import com.kk.mario.states.game.entities.mario.Mario;
import com.kk.mario.states.game.entities.mario.MarioInputFaker;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.BufferedWriter;
import java.lang.reflect.InvocationTargetException;

public class Simulator {
    private LevelState level;
    private boolean simulating = false;
    private ScriptEngine engine = null;
    private API api = null;

    public Simulator(LevelState level) {
        this.level = level;
    }

    public void update() {
        if (this.simulating) {
            for (Enemy enemy : this.getLevel().getEnemies())
                enemy.update();

            this.callFunction("update");

            this.getLevel().getMario().update(this.getLevel().getEnemies());

            if (Gdx.input.isKeyJustPressed(Input.Keys.HOME)) {
                endSimulating();
            }
        } else {
            if (Gdx.input.isKeyJustPressed(Input.Keys.HOME)) {
                beginSimulating();
            }
        }
    }

    private void beginSimulating() {
        if (!(this.getMario().getInputInterface() instanceof MarioInputFaker))
            return;

        this.simulating = true;
        this.getEditor().setEditeable(false);
        this.getEditor().setOpacity(0.0f);

        String source = this.getEditor().getContentsAsString();

        ScriptEngineManager manager = new ScriptEngineManager();
        api = new API(this);
        engine = manager.getEngineByName("JavaScript");
        engine.put("API", api);

        try {
            engine.eval(source);
        } catch (ScriptException exception) {
            this.endSimulating(false);
            exception.printStackTrace();
        }

        this.callFunction("init");
    }

    private void callFunction(String function) {
        if (!this.simulating)
            return;

        try {
            Invocable inv = (Invocable) engine;
            inv.invokeFunction(function);
        } catch (NoSuchMethodException exception) {

        } catch (ScriptException exception) {
            this.endSimulating(false);
            exception.printStackTrace();
        }
    }

    private void endSimulating() {
        this.endSimulating(true);
    }

    private void endSimulating(boolean alertScript) {
        if (!this.simulating)
            return;

        if (alertScript)
            this.callFunction("dispose");

        this.engine = null;
        this.simulating = false;
        this.getEditor().setEditeable(true);
        this.getEditor().setOpacity(1.0f);

        if (alertScript)
            try {
                GameState gameState = MarioGame.getInstance().getGameStateManager().getActiveGameState();

                MarioGame.getInstance().getGameStateManager().markForChange();
                MarioGame.getInstance().getGameStateManager().setActiveGameState(
                        gameState.getClass().getConstructor().newInstance()
                );
            } catch (NoSuchMethodException | InstantiationException | InvocationTargetException | IllegalAccessException exception) {
                exception.printStackTrace();
            }
    }

    public Editor getEditor() {
        return this.getLevel().getEditor();
    }

    public Mario getMario() {
        return this.getLevel().getMario();
    }

    public MarioInputFaker getMarioInputs() {
        return (MarioInputFaker) this.getLevel().getMario().getInputInterface();
    }

    public boolean isSimulating() {
        return simulating;
    }

    public LevelState getLevel() {
        return level;
    }

    public API getApi() {
        return api;
    }
}
