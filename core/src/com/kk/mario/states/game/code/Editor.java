package com.kk.mario.states.game.code;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.kk.mario.rendering.text.Font;
import com.kk.mario.states.game.LevelState;

import java.io.*;
import java.util.ArrayList;

public class Editor implements InputProcessor {
    private final LevelState level;
    private ShapeRenderer shapeRenderer;
    private GlyphLayout glyphLayout = new GlyphLayout();
    private String loadFile;

    private Font titleFont;
    private Font textFont;
    private float height = 620;
    private float width = 1180;
    private float opacity = 1f;
    private ArrayList<String> contents;
    private int cursorX = 0;
    private int cursorY = 0;
    private int scrollY = 0;
    private boolean cursorTickVisible = true;
    private boolean editeable = true;

    public Editor(LevelState level, String loadFile) {
        this(level, loadFile, new ArrayList<String>());
    }

    public Editor(LevelState level, String loadFile, ArrayList<String> contents) {
        this.level = level;
        this.loadFile = loadFile;
        this.contents = contents;
        this.shapeRenderer = new ShapeRenderer();

        this.titleFont = new Font(Gdx.files.internal("fonts/SuperMario256.ttf"), 40, Color.WHITE);
        this.textFont = new Font(Gdx.files.internal("fonts/Courier.ttf"), 20, Color.BLACK);

        this.loadContents();

        if (this.contents.isEmpty())
            this.contents.add("");

        Gdx.input.setInputProcessor(this);
    }

    private void loadContents() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(this.loadFile));

            String line = reader.readLine();
            while (line != null) {
                this.contents.add(line);

                line = reader.readLine();
            }

            reader.close();
        } catch (IOException exception) {

        }
    }

    private void saveContents() {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(this.loadFile));

            for (String line : this.contents) {
                writer.write(line + "\n");
            }

            writer.close();
        } catch (IOException exception) {
            System.out.println("Nepavyko išsaugoti failan!");
        }
    }

    private float deltaTick = 0;
    private float deltaInputTick = 0;

    public void update() {
        deltaTick += Gdx.graphics.getDeltaTime();

        if (deltaTick >= 0.5) {
            deltaTick -= 0.5;

            this.cursorTickVisible = !this.cursorTickVisible;
        }

        if (this.editeable) {
            deltaInputTick += Gdx.graphics.getDeltaTime();

            if (deltaInputTick >= 0.25) {
                deltaInputTick -= 0.25;

                if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
                    this.moveCursorLeft();
                }

                if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
                    this.moveCursorRight();
                }

                if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
                    this.moveCursorUp();
                }

                if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
                    this.moveCursorDown();
                }
            }
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.PLUS)) {
            this.increaseOpacity();
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.MINUS)) {
            this.decreaseOpacity();
        }
    }

    public void render(SpriteBatch batch) {
        //Transparent.
        Gdx.gl.glEnable(Gdx.gl.GL_BLEND);
        Gdx.gl.glBlendFunc(Gdx.gl.GL_SRC_ALPHA, Gdx.gl.GL_ONE_MINUS_SRC_ALPHA);

        float originX = this.calculateOriginX();
        float originY = this.calculateOriginY();
        int lineCount = this.calculateLineCount();
        int lineFrom = this.scrollY;
        int lineTo = lineFrom + lineCount;
        if (cursorY < lineFrom)
            this.scrollY = cursorY;

        else if (cursorY >= lineTo)
            this.scrollY += cursorY - lineTo + 1;

        this.shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(new Color(0, 1, 1, opacity));
        shapeRenderer.rect(originX, originY + this.height - 50, this.width, 50);
        shapeRenderer.setColor(new Color(1, 1, 1, opacity));
        shapeRenderer.rect(originX, originY, this.width, this.height - 50);

        this.shapeRenderer.end();

        Gdx.gl.glDisable(Gdx.gl.GL_BLEND);

        batch.begin();
        if (opacity != 0) {
            this.titleFont.getInstance().draw(batch, "Kodas", originX + 10, originY + this.height - 10);

            float currentY = originY + this.height - 60;

            glyphLayout.setText(this.textFont.getInstance(), new Integer(lineCount).toString());
            float maxNumW = glyphLayout.width;
            for (int i = 0; i < lineCount; i++) {
                try {
                    String line = this.contents.get(i + scrollY);

                    String val = new Integer(i + scrollY + 1).toString();
                    glyphLayout.setText(this.textFont.getInstance(), val);

                    this.textFont.getInstance().draw(batch, val, originX + 10 - glyphLayout.width + maxNumW, currentY);
                    this.textFont.getInstance().draw(batch, line, originX + maxNumW + 30, currentY);
                } catch (IndexOutOfBoundsException exception) {

                } finally {
                    currentY -= 20;
                }
            }

            if (this.cursorTickVisible) {
                String textToLeft = this.getCurrentLine().substring(0, this.cursorX);
                glyphLayout.setText(this.textFont.getInstance(), textToLeft);
                float fromLeft = glyphLayout.width;

                this.textFont.getInstance().draw(batch, "_", originX + maxNumW + 30 + fromLeft, originY + this.height - 60 - (20 * (cursorY - scrollY)));
            }
        }
        batch.end();
    }

    public String getContentsAsString() {
        StringBuilder builder = new StringBuilder();

        for (String line : this.contents) {
            builder.append(line + "\n");
        }

        return builder.toString();
    }

    public void dispose() {
        this.saveContents();
    }

    private float calculateOriginX() {
        return (1280 - this.width) / 2.f;
    }

    private float calculateOriginY() {
        return (720 - this.height) / 2.f;
    }

    private int calculateLineCount() {
        return (int) Math.floor((this.height - 70) / 20);
    }

    private void increaseOpacity() {
        float opacity = this.opacity;
        opacity += 0.2f;
        this.opacity = Math.min(opacity, 1);
    }

    private void decreaseOpacity() {
        float opacity = this.opacity;
        opacity -= 0.2f;
        this.opacity = Math.max(opacity, 0);
    }

    private void moveCursorLeft() {
        if (this.cursorX == 0) {
            this.moveCursorUp();
            this.clampToEnd();
        } else this.cursorX--;
    }

    private void moveCursorRight() {
        if (this.cursorX >= this.getCurrentLine().length()) {
            this.cursorX = 0;
            this.moveCursorDown();
        } else this.cursorX++;
    }

    private void moveCursorUp() {
        if (this.cursorY == 0)
            this.cursorY = this.contents.size() - 1;

        else
            this.cursorY--;

        this.clampToEndIfOver();
    }

    private void moveCursorDown() {
        if (this.cursorY == this.contents.size() - 1)
            this.cursorY = 0;

        else
            this.cursorY++;

        this.clampToEndIfOver();
    }

    private void clampToEndIfOver() {
        if (this.getCurrentLine().length() <= this.cursorX)
            this.clampToEnd();
    }

    private void clampToStart() {
        this.cursorX = 0;
    }

    private void clampToEnd() {
        this.cursorX = this.getCurrentLine().length();
    }

    private void splitCurrentLine() {
        String currLine = this.getCurrentLine().substring(0, this.cursorX);
        String nextLine = this.getCurrentLine().substring(this.cursorX);

        this.contents.set(this.cursorY, currLine);
        this.contents.add(this.cursorY + 1, nextLine);

        this.cursorY += 1;
        this.clampToStart();
    }

    private void removeSymbol() {
        if (this.cursorX == 0) {
            if (this.cursorY == 0)
                return;

            String prevLine = this.contents.get(this.cursorY - 1);
            String currLine = this.getCurrentLine();
            this.contents.set(this.cursorY - 1, prevLine + currLine);
            this.contents.remove(this.cursorY);

            this.cursorY--;
            this.cursorX = prevLine.length();
        } else {
            String currLine = this.getCurrentLine();
            this.contents.set(this.cursorY, currLine.substring(0, cursorX - 1) + currLine.substring(cursorX));
            this.cursorX--;
        }
    }

    private void removeSymbolReverse() {
        String currLine = this.getCurrentLine();

        if (this.cursorX == currLine.length()) {
            if (this.cursorY == this.contents.size() - 1)
                return;

            String nextLine = this.contents.get(this.cursorY + 1);
            this.contents.set(this.cursorY, currLine + nextLine);
            this.contents.remove(this.cursorY + 1);
        } else {
            this.contents.set(this.cursorY, currLine.substring(0, cursorX) + currLine.substring(cursorX + 1));
        }
    }

    public boolean isEditeable() {
        return editeable;
    }

    public void setEditeable(boolean editeable) {
        this.editeable = editeable;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getOpacity() {
        return opacity;
    }

    public void setOpacity(float opacity) {
        this.opacity = opacity;
    }

    private String getCurrentLine() {
        return this.contents.get(this.cursorY);
    }

    public LevelState getLevel() {
        return level;
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        if (!this.editeable)
            return false;

        if (character == 0) {
            if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
                this.moveCursorLeft();
            }

            if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
                this.moveCursorRight();
            }

            if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
                this.moveCursorUp();
            }

            if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
                this.moveCursorDown();
            }

            this.deltaInputTick = 0;

            return false;
        }

        if (character == 13) {
            this.splitCurrentLine();
            return false;
        }

        if (character == 8) {
            this.removeSymbol();
            return false;
        }

        if (character == 127) {
            this.removeSymbolReverse();
            return false;
        }

        String characters = "" + character;
        if (character == 9)
            characters = "    ";

        String currLine = this.getCurrentLine();
        this.contents.set(this.cursorY, currLine.substring(0, cursorX) + characters + currLine.substring(cursorX));
        this.cursorX += characters.length();

        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
