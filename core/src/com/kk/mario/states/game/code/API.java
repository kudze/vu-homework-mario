package com.kk.mario.states.game.code;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

public class API {
    private final Simulator simulator;

    public API(Simulator simulator)
    {
        this.simulator = simulator;
    }

    public void print(String message)
    {
        System.out.println(message);
    }

    public void setMarioGoLeft(boolean enabled) {
        this.simulator.getMarioInputs().setLeftPressed(enabled);
    }

    public void setMarioGoRight(boolean enabled) {
        this.simulator.getMarioInputs().setRightPressed(enabled);
    }

    private Vector2 stabilizePoint = null;
    public void stabilize() {
        this.stabilizePoint = new Vector2(
                this.getMarioX(),
                this.getMarioY()
        );

        this.setMarioGoLeft(false);
        this.setMarioGoRight(false);
    }

    public void stabilizeEnd() {
        this.stabilizePoint = null;
    }

    public boolean hasStabilizedPoint() {
        return this.stabilizePoint != null;
    }

    public float getStabilizedPointX() {
        if(this.stabilizePoint == null)
            return 0.f;

        return this.stabilizePoint.x;
    }

    public float getStabilizedPointY() {
        if(this.stabilizePoint == null)
            return 0.f;

        return this.stabilizePoint.y;
    }

    public void marioJump() {
        this.simulator.getMario().tryToJump();
    }

    public boolean isMarioGoingLeft() {
        return this.simulator.getMarioInputs().isPressingLeft();
    }

    public boolean isMarioGoingRight() {
        return this.simulator.getMarioInputs().isPressingRight();
    }

    public float getMarioX() {
        return this.simulator.getMario().getPosition().x / 16.f;
    }

    public float getMarioY() {
        return this.simulator.getMario().getPosition().y / 16.f;
    }

    public float getMarioVelocityX() {
        return this.simulator.getMario().getForce().x;
    }

    public float getMarioVelocityY() {
        return this.simulator.getMario().getForce().y;
    }

    public boolean canGoLeft() {
        return this.simulator.getMario().canGoLeft();
    }

    public boolean canGoRight() {
        return this.simulator.getMario().canGoRight();
    }

    public boolean canJump() {
        return !this.simulator.getMario().canGoDown();
    }

    public boolean isOnGround() {
        return this.canJump() && this.getMarioVelocityY() <= 0;
    }

    public boolean canFall() {
        return this.simulator.getMario().canGoDown();
    }

    public float getDeltaTime() { return Gdx.graphics.getDeltaTime(); }
}
