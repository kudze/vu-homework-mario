package com.kk.mario.states.game.map;

import com.badlogic.gdx.graphics.g2d.Batch;

public class Map {

    private Block[][] tiles;

    public Map(int width, int height)
    {
        this.tiles = new Block[width][height];
    }

    public void render(Batch batch)
    {
        for(int x = 0; x < tiles.length; x++)
            for(int y = 0; y < tiles[x].length; y++)
                if(this.hasBlockAtXY(x, y))
                    this.getBlockAtXY(x, y).getSprite().draw(batch);
    }

    public boolean hasBlockAtXY(int x, int y) {
        return this.tiles[x][y] != null;
    }

    public boolean isBlockAtXYWithCollision(int x, int y)
    {
        Block block = this.getBlockAtXY(x, y);

        if(block == null)
            return false;

        return block.hasCollision();
    }

    public Block getBlockAtXY(int x, int y) {
        if(x < 0 || x >= this.tiles.length)
            return null;

        if(y < 0 || y >= this.tiles[x].length)
            return null;

        return this.tiles[x][y];
    }

    public void setBlockAtXY(int x, int y, Block block)
    {
        this.tiles[x][y] = block;

        if(block != null)
            block.setMapCoordinates(this, x, y);
    }

    public Block[][] getTiles() {
        return tiles;
    }

    public void setTiles(Block[][] tiles) {
        this.tiles = tiles;
    }

    public int getHeight() {
        return this.tiles[0].length;
    }

    public int getWidth() {
        return this.tiles.length;
    }
}
