package com.kk.mario.states.game.map;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.kk.mario.states.game.entities.mario.Mario;

public abstract class Block {
    private Sprite sprite;
    private boolean collision;
    private Map map;
    private int mapX, mapY;

    public Block(Sprite sprite) {
        this(sprite, true);
    }

    public Block(Sprite sprite, boolean collision) {
        this.sprite = sprite;
        this.collision = collision;
    }

    public void onMarioTouch(Mario mario) {};
    public void onMarioHit(Mario mario) {};

    void setMapCoordinates(Map map, int x, int y) {
        this.map = map;
        this.mapX = x;
        this.mapY = y;

        this.getSprite().setPosition(x * 16, y * 16);
    }

    public void setSprite(Sprite sprite) {
        this.sprite = sprite;
        this.sprite.setPosition(this.mapX * 16, this.mapY * 16);
    }

    public Sprite getSprite() {
        return sprite;
    }

    public boolean hasCollision() {
        return collision;
    }

    public Map getMap() {
        return map;
    }

    public int getMapX() {
        return mapX;
    }

    public int getMapY() {
        return mapY;
    }
}
