package com.kk.mario.states.game.map;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;

public class Brick extends Block {

    public Brick(TextureAtlas textureAtlas)
    {
        super(textureAtlas.createSprite("brick"), true);
    }

}
