package com.kk.mario.states.game.map;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.kk.mario.states.game.entities.mario.Mario;

public class Coin extends Block {

    public Coin(TextureAtlas textureAtlas)
    {
        super(textureAtlas.createSprite("coin"), false);
    }

    @Override
    public void onMarioTouch(Mario mario) {
        mario.setCoins(mario.getCoins() + 1);
        this.getMap().setBlockAtXY(
                this.getMapX(),
                this.getMapY(),
                null
        );
    }

}
