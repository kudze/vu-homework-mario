package com.kk.mario.states.game.map;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.kk.mario.states.game.entities.mario.Mario;

public class Question extends Block {
    private TextureAtlas textureAtlas;
    private boolean isHit = false;

    public Question(TextureAtlas textureAtlas)
    {
        super(textureAtlas.createSprite("question"), true);

        this.textureAtlas = textureAtlas;
    }

    @Override
    public void onMarioHit(Mario mario) {
        if(this.isHit)
            return;

        this.setSprite(this.textureAtlas.createSprite("plate"));
        this.getMap().setBlockAtXY(
                this.getMapX(),
                this.getMapY() + 1,
                new Coin(this.textureAtlas)
        );
        this.isHit = true;
    }
}
