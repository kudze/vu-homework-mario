package com.kk.mario.states.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.kk.mario.rendering.text.Font;
import com.kk.mario.states.GameState;
import com.kk.mario.states.game.code.Editor;
import com.kk.mario.states.game.code.Simulator;
import com.kk.mario.states.game.entities.enemies.Enemy;
import com.kk.mario.states.game.entities.mario.Mario;
import com.kk.mario.states.game.map.Map;

import java.util.ArrayList;

public abstract class LevelState implements GameState
{
    private Map map;
    private Camera camera;
    private TextureAtlas blocksTextureAtlas;
    private TextureAtlas enemiesTextureAtlas;
    private SpriteBatch batch;
    private SpriteBatch UIBatch;
    private SpriteBatch editorBatch;
    private Mario mario;
    private ArrayList<Enemy> enemies = new ArrayList<>();
    private Font font;
    private Editor editor;
    private Simulator simulator;

    protected LevelState(Map map, int marioSpawnX, int marioSpawnY, String levelName) {
        this.map = map;
        this.mario = new Mario(marioSpawnX, marioSpawnY, this);

        this.camera = new OrthographicCamera(284, 160);
        this.camera.translate(142, 80, 0);

        this.blocksTextureAtlas = new TextureAtlas(Gdx.files.internal("blocks/sprites.txt"));
        this.enemiesTextureAtlas = new TextureAtlas(Gdx.files.internal("enemies/sprites.txt"));
        this.batch = new SpriteBatch();

        this.UIBatch = new SpriteBatch();
        this.font = new Font(Gdx.files.internal("fonts/SuperMario256.ttf"), 50, Color.WHITE, Color.BLACK, 2);

        this.editorBatch = new SpriteBatch();

        this.editor = new Editor(this, levelName + ".js");
        this.simulator = new Simulator(this);
    }

    @Override
    public void update() {
        this.updateCamera();

        editor.update();
        simulator.update();
    }

    @Override
    public void render() {
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        this.map.render(batch);

        for(Enemy enemy : this.enemies)
            enemy.render(batch);

        this.mario.render(batch);
        batch.end();

        UIBatch.begin();
        this.font.getInstance().draw(UIBatch, "Coins: " + this.mario.getCoins(), 5, 715);
        UIBatch.end();

        this.editor.render(editorBatch);
    }

    @Override
    public void dispose() {
        this.batch.dispose();
        this.blocksTextureAtlas.dispose();
        this.mario.dispose();
        this.font.dispose();
        this.UIBatch.dispose();
        this.editorBatch.dispose();
        this.editor.dispose();
    }

    protected void updateCamera() {
        this.camera.position.x = Math.min(Math.max(142, this.mario.getPosition().x), (this.getMap().getWidth() * 16) - (this.camera.viewportWidth / 2));
        this.camera.position.y = Math.min(Math.max(80, this.mario.getPosition().y), (this.getMap().getHeight() * 16) - (this.camera.viewportHeight / 2));

        this.camera.update();
    }

    public Map getMap() {
        return map;
    }

    public Camera getCamera() {
        return camera;
    }

    public TextureAtlas getBlocksTextureAtlas() {
        return blocksTextureAtlas;
    }

    public TextureAtlas getEnemiesTextureAtlas() {
        return enemiesTextureAtlas;
    }

    public SpriteBatch getBatch() {
        return batch;
    }

    public Mario getMario() {
        return mario;
    }

    public ArrayList<Enemy> getEnemies() {
        return enemies;
    }

    public void addEnemy(Enemy enemy) {
        this.enemies.add(enemy);
    }

    public void removeEnemy(Enemy enemy) {
        this.enemies.remove(enemy);
    }

    public Editor getEditor() {
        return editor;
    }

    public Simulator getSimulator() {
        return simulator;
    }
}
