package com.kk.mario.states.game.entities.enemies;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.kk.mario.states.game.map.Block;

import java.util.ArrayList;

public class Mushroom extends Enemy {

    private Sprite mushroomDead;
    private boolean dead = false;

    private ArrayList<Sprite> mushroomWalkingFrames;
    private float walkingAnimTime = 0;

    public Mushroom(TextureAtlas enemyTextureAtlas, int spawnX, int spawnY) {
        super(16, 16, spawnX, spawnY);

        this.mushroomDead = enemyTextureAtlas.createSprite("mushroom_dead");
        this.mushroomWalkingFrames = new ArrayList<>();
        this.mushroomWalkingFrames.add(enemyTextureAtlas.createSprite("mushroom_walk_1"));
        this.mushroomWalkingFrames.add(enemyTextureAtlas.createSprite("mushroom_walk_2"));

        this.goLeft();
    }

    @Override
    public void render(Batch batch) {
        if (!this.dead) {
            this.walkingAnimTime += Gdx.graphics.getDeltaTime();
            int frame = ((int) Math.round(walkingAnimTime * 5)) % this.mushroomWalkingFrames.size();
            Sprite walkingSprite = this.mushroomWalkingFrames.get(frame);

            this.renderSprite(batch, walkingSprite);
        } else {
            this.renderSprite(batch, this.mushroomDead);
        }
    }

    @Override
    public void dispose() {

    }

    @Override
    protected void collidedWithBlock(Block block, HitDirection direction) {
        if (direction == HitDirection.LEFT)
            this.goRight();

        if (direction == HitDirection.RIGHT)
            this.goLeft();
    }

    public void goLeft() {
        Vector2 force = this.getForce();
        force.x = -20;
        this.setDirection(Direction.LEFT);
        this.setForce(force);
    }

    public void goRight() {
        Vector2 force = this.getForce();
        force.x = 20;
        this.setDirection(Direction.RIGHT);
        this.setForce(force);
    }

    @Override
    public void die() {
        this.dead = true;
        this.setForce(new Vector2());
    }

    @Override
    public boolean isDead() {
        return this.dead;
    }
}
