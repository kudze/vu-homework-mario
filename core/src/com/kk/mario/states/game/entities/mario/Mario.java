package com.kk.mario.states.game.entities.mario;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.kk.mario.states.game.LevelState;
import com.kk.mario.states.game.entities.Entity;
import com.kk.mario.states.game.entities.enemies.Enemy;
import com.kk.mario.states.game.map.Block;
import com.kk.mario.states.game.map.Map;

import java.util.ArrayList;
import java.util.List;

public class Mario extends Entity {
    private Sprite marioIdle;
    private Sprite marioJump;
    private Sprite marioFlag;
    private Sprite marioSlide;

    private ArrayList<Sprite> marioWalking = new ArrayList<>();
    private float walkAnimTime = 0;

    private Sprite marioDeath;
    private boolean dead = false;
    private boolean isOnFlag = false;

    private TextureAtlas marioTextures;
    private int coins = 0;

    private LevelState level;

    private MarioInputInterface inputInterface;

    public Mario(int spawnX, int spawnY, LevelState level)
    {
        this(spawnX, spawnY, new MarioInputFaker(null), level);
    }

    public Mario(int spawnX, int spawnY, MarioInputInterface inputInterface, LevelState level) {
        super(16, 16, spawnX, spawnY);

        this.inputInterface = inputInterface;
        this.level = level;
        if(inputInterface instanceof MarioInputFaker) {
            MarioInputFaker inputFakerInterface = (MarioInputFaker) inputInterface;
            inputFakerInterface.setMario(this);
        }

        this.marioTextures = new TextureAtlas(Gdx.files.internal("mario/sprites.txt"));
        this.marioIdle = this.marioTextures.createSprite("mario_idle");
        this.marioJump = this.marioTextures.createSprite("mario_jump");
        this.marioDeath = this.marioTextures.createSprite("mario_death");
        this.marioFlag = this.marioTextures.createSprite("mario_flag");
        this.marioSlide = this.marioTextures.createSprite("mario_slide");
        this.marioWalking.add(this.marioTextures.createSprite("mario_run_1"));
        this.marioWalking.add(this.marioTextures.createSprite("mario_run_2"));
        this.marioWalking.add(this.marioTextures.createSprite("mario_run_3"));
    }

    public void update(List<Enemy> enemyList) {
        Vector2 position = this.getPosition();
        Vector2 force = this.getForce();

        if (this.dead) {
            force.y = Math.max(force.y - (Gdx.graphics.getDeltaTime() * 40), -40);
            position.y += force.y * Gdx.graphics.getDeltaTime() * 5;

            this.setPosition(position);
            this.setForce(force);

            return;
        }

        if (this.isOnFlag) {
            position.y += force.y * Gdx.graphics.getDeltaTime() * 5;

            this.setPosition(position);
            this.setForce(force);

            return;
        }

        this.updateTouchedEnemies(enemyList);
        this.updateTouchedBlocks();

        //Running left & right.
        if (this.inputInterface.isPressingLeft() && this.canGoLeft()) {
            force.x = Math.max(force.x - (Gdx.graphics.getDeltaTime() * 120), -60);

            if (this.getDirection() != Direction.LEFT) {
                this.setDirection(Direction.LEFT);
                this.walkAnimTime = 0;
            }
        }

        if (this.inputInterface.isPressingRight() && this.canGoRight()) {
            force.x = Math.min(force.x + (Gdx.graphics.getDeltaTime() * 120), 60);

            if (this.getDirection() != Direction.RIGHT) {
                this.setDirection(Direction.RIGHT);
                this.walkAnimTime = 0;
            }
        }

        super.update();

        force = this.getForce();

        if (!this.inputInterface.isPressingLeft() && !this.inputInterface.isPressingRight() && force.x != 0) {
            if (force.x < 1 && force.x > -1)
                force.x = 0;

            else if (force.x > 0)
                force.x -= Gdx.graphics.getDeltaTime() * 80;

            else
                force.x += Gdx.graphics.getDeltaTime() * 80;
        }

        //Gravity.
        if (this.inputInterface.isPressingJump() && !this.canGoDown()) {
            force.y = 170;
        }

        this.setForce(force);

        if (position.y < -16)
            this.die();
    }

    public void tryToJump() {
        if (!this.canGoDown()) {
            Vector2 force = this.getForce();
            force.y = 170;
            this.setForce(force);
        }
    }

    @Override
    protected void collidedWithBlock(Block block, HitDirection direction) {
        if (direction == HitDirection.UP && block != null)
            block.onMarioHit(this);
    }

    @Override
    public void render(Batch batch) {
        if (this.dead) {
            this.renderSprite(batch, this.marioDeath);
        } else if (this.isOnFlag) {
            this.renderSprite(batch, this.marioFlag);
        } else if (this.getForce().y != 0) {
            this.renderSprite(batch, this.marioJump);
        } else if ((Gdx.input.isKeyPressed(Input.Keys.A) && this.getForce().x > 0) || (Gdx.input.isKeyPressed(Input.Keys.D) && this.getForce().x < 0)) {
            this.renderSprite(batch, this.marioSlide);
        } else if (this.getForce().x != 0) {
            this.walkAnimTime += Gdx.graphics.getDeltaTime();
            int drawedFrame = ((int) Math.floor(this.walkAnimTime * 6)) % this.marioWalking.size();
            Sprite drawedAnimFrame = this.marioWalking.get(drawedFrame);

            this.renderSprite(batch, drawedAnimFrame);
        } else {
            this.renderSprite(batch, this.marioIdle);
        }
    }

    public void die() {
        if (this.dead == false) {
            this.dead = true;

            Vector2 force = this.getForce();
            force.y = 40;
            this.setForce(force);
        }
    }

    public void touchedFlag() {
        if (this.isOnFlag == false) {
            Vector2 force = this.getForce();
            force.y = -5;
            this.setForce(force);
            this.isOnFlag = true;
        }
    }

    public void dispose() {
        this.marioTextures.dispose();
    }

    private void updateTouchedEnemies(List<Enemy> enemies) {

        for(Enemy enemy : enemies)
            if(!enemy.isDead() && enemy.hasCollidedWithMario(this)) {
                if(this.getForce().y < 0) {
                    enemy.die();

                    Vector2 force = this.getForce();
                    force.y += 50;
                    this.setForce(force);
                }
                else
                    this.die();
            }

    }

    private void updateTouchedBlocks() {
        Vector2 position = this.getPosition();
        int y1 = (int) Math.floor(position.y / 16);
        int y2 = (int) Math.ceil(position.y / 16);
        int x1 = (int) Math.floor(position.x / 16);
        int x2 = (int) Math.ceil(position.x / 16);

        Map map = this.getMap();
        Block block = map.getBlockAtXY(x1, y1);
        if (block != null) block.onMarioTouch(this);

        Block block1 = map.getBlockAtXY(x1, y2);
        if (block1 != null) block1.onMarioTouch(this);

        Block block2 = map.getBlockAtXY(x2, y1);
        if (block2 != null) block2.onMarioTouch(this);

        Block block3 = map.getBlockAtXY(x2, y2);
        if (block3 != null) block3.onMarioTouch(this);
    }

    public boolean isDead() {
        return dead;
    }

    public boolean isOnFlag() {
        return isOnFlag;
    }

    public int getCoins() {
        return coins;
    }

    public void setCoins(int coins) {
        this.coins = coins;
    }

    public MarioInputInterface getInputInterface() {
        return inputInterface;
    }

    public LevelState getLevel() {
        return level;
    }
}
