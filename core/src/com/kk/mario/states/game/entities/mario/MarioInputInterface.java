package com.kk.mario.states.game.entities.mario;

public interface MarioInputInterface {
    boolean isPressingLeft();
    boolean isPressingRight();
    boolean isPressingJump();
}
