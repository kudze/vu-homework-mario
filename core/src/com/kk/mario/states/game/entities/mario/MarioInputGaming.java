package com.kk.mario.states.game.entities.mario;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;

public class MarioInputGaming implements MarioInputInterface {

    @Override
    public boolean isPressingLeft() {
        return Gdx.input.isKeyPressed(Input.Keys.A);
    }

    @Override
    public boolean isPressingRight() {
        return Gdx.input.isKeyPressed(Input.Keys.D);
    }

    @Override
    public boolean isPressingJump() {
        return Gdx.input.isKeyPressed(Input.Keys.SPACE);
    }
}
