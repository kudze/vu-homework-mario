package com.kk.mario.states.game.entities.enemies;

import com.kk.mario.states.game.entities.Entity;
import com.kk.mario.states.game.entities.mario.Mario;

public abstract class Enemy extends Entity {
    public Enemy(int width, int height, int spawnX, int spawnY) {
        super(width, height, spawnX, spawnY);
    }

    public abstract void die();
    public abstract boolean isDead();

    public boolean hasCollidedWithMario(Mario mario) {
        float leftX = this.getPosition().x;
        float rightX = leftX + this.getWidth();
        float downY = this.getPosition().y;
        float upY = downY + this.getHeight();

        float mLeftX = mario.getPosition().x;
        float mRightX = mLeftX + mario.getWidth();
        float mDownY = mario.getPosition().y;
        float mUpY = mDownY + mario.getHeight();

        return leftX < mRightX && rightX > mLeftX && downY < mUpY && upY > mDownY;
    }
}
