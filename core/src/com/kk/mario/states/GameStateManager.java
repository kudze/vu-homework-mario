package com.kk.mario.states;

public class GameStateManager {
    private GameState activeGameState = null;

    public GameStateManager(GameState initialState) {
        this.activeGameState = initialState;
    }

    public GameState getActiveGameState() {
        return activeGameState;
    }

    public void dispose() {
        this.activeGameState.dispose();
    }

    public void markForChange() {
        if (this.activeGameState != null) {
            this.activeGameState.dispose();
            this.activeGameState = null;
        }
    }

    public void setActiveGameState(GameState activeGameState) {
        GameState oldGameState = this.activeGameState;
        this.activeGameState = activeGameState;

        if (oldGameState != null)
            oldGameState.dispose();
    }
}
