package com.kk.mario.rendering.image;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

public class Picture {
    private Texture texture;
    private Image instance;

    public Picture(FileHandle fileHandle)
    {
        this.texture = new Texture(fileHandle);
        this.texture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        this.instance = new Image(texture);
    }

    public Picture(FileHandle fileHandle, int width, int height)
    {
        this(fileHandle);

        float scaleX = width / this.instance.getWidth();
        float scaleY = height / this.instance.getHeight();

        this.instance.setScaleX(scaleX);
        this.instance.setScaleY(scaleY);
    }

    public void dispose() {
        this.texture.dispose();
    }

    public Image getInstance() {
        return instance;
    }
}
